export interface Empresa {
    id: number;
    nombre: string;
    ciudad: string;
    fecha: string;
    producto: string;
    cantidad: number;
    correo: string;
    telefono: number;
    contacto: string;
}
