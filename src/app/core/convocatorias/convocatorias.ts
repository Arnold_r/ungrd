import { Convocatoria } from './convocatoria';

export const CONVOCATORIAS: Convocatoria[] =[
    {
        "id": "1",
        "producto": "Tapabocas N95",
        "fechaFin": "4/13/20",
        "tipoContrato": "Mixto",
        "formaPago": "30 días",
        "plazo": "4/13/20",
        "periodoEntrega": "Anticipado",
        "riesgos": "Si",
        "lugar": "Zona centro"
    },
    {
        "id": "2",
        "producto": "Guantes de vinilo",
        "fechaFin": "4/14/20",
        "tipoContrato": "Público",
        "formaPago": "60 días",
        "plazo": "4/14/20",
        "periodoEntrega": "Por demanda",
        "riesgos": "No",
        "lugar": "Zona sur"
    },
    {
        "id": "3",
        "producto": "Gorros",
        "fechaFin": "6/15/20",
        "tipoContrato": "Público",
        "formaPago": "120 días",
        "plazo": "6/15/20",
        "periodoEntrega": "Por demanda",
        "riesgos": "No",
        "lugar": "Zona oriental"
    },
    {
        "id": "4",
        "producto": "Gafas",
        "fechaFin": "4/16/20",
        "tipoContrato": "Mixto",
        "formaPago": "Contado",
        "plazo": "4/16/20",
        "periodoEntrega": "Por demanda",
        "riesgos": "No",
        "lugar": "Zona occidental"
    },
    {
        "id": "5",
        "producto": "Gafas",
        "fechaFin": "9/17/20",
        "tipoContrato": "Público",
        "formaPago": "30 días",
        "plazo": "9/17/20",
        "periodoEntrega": "Por demanda",
        "riesgos": "Si",
        "lugar": "Zona occidental"
    },
    {
        "id": "6",
        "producto": "Bata Manga Larga",
        "fechaFin": "4/18/20",
        "tipoContrato": "Público",
        "formaPago": "60 días",
        "plazo": "4/18/20",
        "periodoEntrega": "Anticipado",
        "riesgos": "No",
        "lugar": "Zona occidental"
    },
    {
        "id": "7",
        "producto": "Bata Manga Larga",
        "fechaFin": "5/19/20",
        "tipoContrato": "Público",
        "formaPago": "60 días",
        "plazo": "5/19/20",
        "periodoEntrega": "Anticipado",
        "riesgos": "No",
        "lugar": "Zona oriental"
    },
    {
        "id": "8",
        "producto": "Gafas",
        "fechaFin": "4/20/20",
        "tipoContrato": "Mixto",
        "formaPago": "120 días",
        "plazo": "4/20/20",
        "periodoEntrega": "Por demanda",
        "riesgos": "Si",
        "lugar": "Zona oriental"
    },
    {
        "id": "9",
        "producto": "Tapabocas N95",
        "fechaFin": "5/21/20",
        "tipoContrato": "Mixto",
        "formaPago": "120 días",
        "plazo": "5/21/20",
        "periodoEntrega": "Por demanda",
        "riesgos": "No",
        "lugar": "Zona oriental"
    },
    {
        "id": "10",
        "producto": "Gorros",
        "fechaFin": "4/22/20",
        "tipoContrato": "Público",
        "formaPago": "Contado",
        "plazo": "4/22/20",
        "periodoEntrega": "Anticipado",
        "riesgos": "No",
        "lugar": "Zona Norte"
    }
]

