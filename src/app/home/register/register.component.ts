import { Component, OnInit, ViewChild, ElementRef, ViewChildren, QueryList } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  @ViewChild('labelImport') labelImport: ElementRef;

  @ViewChildren('labelUpload') labelUpload: QueryList<ElementRef>;

  @ViewChildren('labelUploadPr') labelUploadPr: QueryList<ElementRef>;

  registerForm: FormGroup;

  proveedores = [
    'Fabricante Nacional',
    'Comercializadores e importadores de EEPS',
    'Fabricantes Nacionales potenciales',
    'Fabricantes nacionales e importadores por primera vez',
  ];

  listaProductos = [
    'Bata manga larga antifluído o laminada',
    'Gorros',
    'Polainas',
    'Gafas',
    'Guantes de Vinilo',
    'Guantes estériles (Guantes de latex estériles)',
    'Guantes no estériles (Guantes de latex no estériles)',
    'Tapabocas (Mascarilla Quirúrgica)',
    'Tapabocas N95'
  ]

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.registerForm = this.fb.group({
      tipoProveedor: [this.proveedores[0], Validators.required],
      nombre: [, Validators.required],
      mercantilFile: [, Validators.required],
      telefono: [, Validators.required],
      fotocopiaFile: [, Validators.required],
      rut: [, Validators.required],
      parafiscalFile: [, Validators.required],
      rupFile: [, Validators.required],
      inscripcionFile: [, Validators.required],
      cartaFile: [, Validators.required],
      productos: this.fb.array([]),
    });
    this.onChange(null);
    this.addProducto();
  }

  get productos(): FormArray { return this.registerForm.get('productos') as FormArray; }


  addProducto() {
    this.productos.push(this.createProducto());
  }

  createProducto(): FormGroup {
    return this.fb.group({
      producto: [this.listaProductos[0], Validators.required],
      cantidad: [, Validators.required],
      lote: [, Validators.required],
      fabricante: [, Validators.required],
      telefono: [, Validators.required],
      invimaFile: [, Validators.required],
      fichaTecnicaFile: [, Validators.required],
      institucionesFile: [, Validators.required],
    });
  }

  delete(i) {
    this.productos.removeAt(i);
  }

  save(form) {
    if (form.valid) {
      console.log(form.value);
    } else {
      this.markElements(this.registerForm.controls);
    }
  }

  markElements(controls) {
    for (const key in controls) {
      if (controls.hasOwnProperty(key)) {
        const element = controls[key];
        if (element.controls) {
          this.markElements(element.controls)
        }
        element.markAsDirty();
      }
    }
  }


  enabled(controles: Array<string>) {
    controles.forEach(control => {
      this.registerForm.get(control).enable();
      this.registerForm.get(control).setValidators(Validators.required);
    });
  }

  disabled(controles: Array<string>) {
    controles.forEach(control => {
      this.registerForm.get(control).disable();
      this.registerForm.get(control).setValidators(Validators.nullValidator);
    });
  }

  onChange(event) {
    switch (event) {
      case this.proveedores[1]:
      case this.proveedores[3]:
        this.disabled(['parafiscalFile', 'inscripcionFile', 'cartaFile']);
        break;
      case this.proveedores[2]:
        this.enabled(['parafiscalFile', 'inscripcionFile', 'cartaFile']);
        break;
      default:
        this.enabled(['parafiscalFile'])
        this.disabled(['inscripcionFile', 'cartaFile']);
        break;
    }
  }

  onFileChange(files: FileList, index) {
    this.labelUpload.toArray()[index].nativeElement.innerText = Array.from(files)
      .map(f => f.name)
      .join(', ');
  }

  onFileChangePr(files: FileList, index) {
    this.labelUploadPr.toArray()[index].nativeElement.innerText = Array.from(files)
      .map(f => f.name)
      .join(', ');
  }

}
