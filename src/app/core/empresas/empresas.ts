import { Empresa } from './empresa';

export const EMPRESAS: Empresa[] = [
    {
        "id": 1,
        "nombre": "Coopidrogas",
        "ciudad": "Bogota",
        "fecha": "12 de abril",
        "producto": "Bata Manga larga",
        "cantidad": 6780,
        "correo": "aramirez@coopidrogas.com",
        "telefono": 3219877635,
        "contacto": "Jorge Lopez"
    },
    {
        "id": 2,
        "nombre": "3M",
        "ciudad": "Miami",
        "fecha": "17 de abril",
        "producto": "Gorros",
        "cantidad": 28100,
        "correo": "svega@3m.com",
        "telefono": 3119870832,
        "contacto": "Rafael Vanegas"
    },
    {
        "id": 3,
        "nombre": "Fundación Santa Fé",
        "ciudad": "Medellin",
        "fecha": "20 de abril",
        "producto": "Polainas",
        "cantidad": 25300,
        "correo": "fdiaz@fsfe.co",
        "telefono": 3215877635,
        "contacto": "David Zea"
    },
    {
        "id": 4,
        "nombre": "Merydiam",
        "ciudad": "Barranquilla",
        "fecha": "20 de abril",
        "producto": "Gafas",
        "cantidad": 5400,
        "correo": "nbetancur@google.com",
        "telefono": 3219875435,
        "contacto": "Hugo Ramirez"
    },
    {
        "id": 5,
        "nombre": "Healthcare",
        "ciudad": "Barranquilla",
        "fecha": "20 de abril",
        "producto": "Gorros",
        "cantidad": 20000,
        "correo": "luis@healthcare.com",
        "telefono": 3118397635,
        "contacto": "Jorge Vega"
    },
    {
        "id": 6,
        "nombre": "Oxis",
        "ciudad": "Medellin",
        "fecha": "24 de abril",
        "producto": "Tapabocas N95",
        "cantidad": 30000,
        "correo": "gerencia@oxis.com",
        "telefono": 3219877635,
        "contacto": "Matilde Henao"
    },
    {
        "id": 7,
        "nombre": "Drogas la Rebaja",
        "ciudad": "Cali",
        "fecha": "27 de abril",
        "producto": "Gafas",
        "cantidad": 10000,
        "correo": "compras@drogaslarebaja.co",
        "telefono": 3219875434,
        "contacto": "Jhon Ramirez"
    },
    {
        "id": 8,
        "nombre": "Omega ",
        "ciudad": "Buenos aires",
        "fecha": "27 de abril",
        "producto": "Tapabocas N95",
        "cantidad": 120000,
        "correo": "jhonramrez@omega.co",
        "telefono": 3219875435,
        "contacto": "Fernando Diaz"
    },
    {
        "id": 9,
        "nombre": "Salud y bienestar",
        "ciudad": "Bogota",
        "fecha": "27 de abril",
        "producto": "Tapabocas N95",
        "cantidad": 250000,
        "correo": "ventassaludbien@hotmail.com",
        "telefono": 3219877639,
        "contacto": "Luis Lopez"
    },
    {
        "id": 10,
        "nombre": "Sanitas",
        "ciudad": "Bogota",
        "fecha": "30 de abril",
        "producto": "Bata Manga larga",
        "cantidad": 5105,
        "correo": "sparra@keralty.com.co",
        "telefono": 3219877635,
        "contacto": "Raul Moya"
    }
];